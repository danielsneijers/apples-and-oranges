<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<title>Apples and Oranges</title>

	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes">

	<link rel="shortcut icon" href="favicon.ico" />

	<link href="css/style.css" rel="stylesheet" type="text/css" />
	
	<base href="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>" />

	<!--[if lt IE 9]>
		<script src="{{ asset("js/plugins/html5shiv.js") }}"></script>
	<![endif]-->
</head>

<body>

	<?php include('partials/grid.php'); ?>
	
	<?php if($_SERVER['SERVER_NAME'] == 'apples-and-oranges.dev'): ?>
		<script data-main="/js/main" src="/js/bower_components/requirejs/require.js"></script>
	<?php else: ?>
		<script src="/js/built/main.min.js"></script>
		<script type="text/javascript">
			if(!window.console) window.console = {}; var methods = ["log", "debug", "warn", "info"]; for(var i=0;i<methods.length;i++){ console[methods[i]] = function(){};}
		</script>
	<?php endif; ?>

</body>
</html>