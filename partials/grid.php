<div id="container" class="js-masonry">

	<div id="nav" class="grid-sizer">
		<img src="images/logo.svg" alt="Apples and Oranges logo" class="logo" />
	</div>

	<?php
	$path = getcwd();
	$photos = glob($path.'/media/*.{JPG,jpg}', GLOB_BRACE);

	foreach($photos as $photo): 
		
		$link = pathinfo($photo);
		if( substr( $link['filename'], 0, 6 ) !== "thumb_"):
	?>
	
	<div class="item" data-image="<?= $link['filename'] ?>.jpg">
		<button class="hover-btn">
			<span class="zoom-btn"></span>
		</button>
		<div class="image-box"></div>
	</div>

	<?php 
	endif;
	endforeach; 
	?>	

</div>