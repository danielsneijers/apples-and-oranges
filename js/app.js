define(['zepto/zepto',
		'masonry/masonry',
        'widgets/grid',
        'widgets/nav'], 
    function($, Masonry, Grid, Nav) {

    var App = {

        msnry: null,

        init: function(){

            console.log('%c App init' , 'background-color: #ecf0f1; color: #3b3b3b;');

            this.enable();
            this.masonryInit();

            // Load grid Widgets
            Grid.init();
            Nav.init();

            
        },

        enable: function(){
            $(window).on('resize', $.proxy(this, 'resetGrid'));
        },

        masonryInit: function(){

            var container = document.querySelector('#container');
            
            this.msnry = new Masonry( container, {
              columnWidth: '.grid-sizer'
            });
            
            console.log('%c Masonry Init ', 'background-color: #3498db; color: #ffffff;');
        },

        resetGrid: function(){
            
            var self = this;

            console.log('reset!');

            setTimeout(function(){
                self.msnry.layout();
            }, 500);
        }
        
    };
    
    return App; 
});