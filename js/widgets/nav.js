define(['zepto/zepto',
		], 
    function($) {

    var Nav = {
        
        init: function(){

            console.log('%c Nav init ', 'background-color: #ecf0f1; color: #3b3b3b;');

            this.enable();
        },

        enable: function(){
            $('#nav').on('click', '', $.proxy(this, 'openNav'));
        },

        openNav: function(e){

            var nav = $('#nav');

            nav.toggleClass('top-layer fullscreen');
            //$('.options').toggleClass('hidden');
        }

    };
    
    return Nav; 
});