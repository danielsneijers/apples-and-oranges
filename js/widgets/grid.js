define(['zepto/zepto',
		], 
    function($) {

    var Grid = {

        parentElement: null,
        offsetLeft: null,
        offsetTop: null,
        
        init: function(){

            console.log('%c Grid init ', 'background-color: #ecf0f1; color: #3b3b3b;');

            this.enable();
            this.loadGridImages();
        },

        enable: function(){

            $('.hover-btn').on('click', '', $.proxy(this, 'setupImageEffect'));
            $('.item').on('click', '', $.proxy(this, 'resetImageContainers'));
        },

        loadGridImages: function(){

            self = this;

            // Get each data-image from grid items and preload 'em
            $('.item').each(function(){
                var image = $(this).data('image');
                var thumb = "media/thumb_"+image;
                self.preloadThumbnail(thumb, this);
            });
        },

        preloadThumbnail: function(img, target){
            
            var self,
                image;

            self = this;
            image = new Image();
            image.src = img;

            // Place image in item when loaded
            image.onload = function(){
                $(target).css({
                    'background-image': 'url(' + image.src + ')',
                });
            };
        },

        setupImageEffect: function(e){

            console.log('setup image effect:', e);
            // Save parent element in var
            this.parentElement = e.target.parentElement;
            
            // Store correct parentElement as zepto selector
            if( $(this.parentElement).hasClass('hover-btn') ){
                this.parentElement = $(this.parentElement.parentElement);
            } else {
                this.parentElement = $(this.parentElement);
            }
            
            // Set vars for positioning
            this.offsetLeft = this.parentElement.offset().left;
            this.offsetTop = this.parentElement.offset().top ;
    
            // Get image of clicked container
            var imageBox = this.parentElement.find('.image-box');
            var image = "media/" + this.parentElement.data('image');

            // Load the Background
            this.loadLargeBackground(image, imageBox);
            
        },

        loadLargeBackground: function(img, target){

            var self,
                image;

            self = this;
            image = new Image();
            image.src = img;

            // Place image in background when loaded
            image.onload = function(){
                self.setBackgroundImage(image.src, target);
                console.log('%c Image loaded ', 'background-color:#1abc9c; color: white;');
            };
        },

        setBackgroundImage: function(src, target){

            var self = this;
            
            // Set image as background for target
            $(target).css({
                'background-image': 'url(' + src + ')',
            }).addClass('active');

            // Make parent elemant top-layer and hide button
            this.parentElement.addClass('top-layer');
            this.parentElement.find('.hover-btn').addClass('hidden');
            
            // Set timeout to resize container after fade-in{@_grid.scss} finished
            setTimeout(function(){
                self.resizeImageContainer(target);
            }, 700);
            
        },

        resizeImageContainer: function(target){
            
            console.log('%c Resize Img Container ', 'background-color: #3498db; color: #ffffff;');

            // Prevent body from scrolling when fullscreen
            $('body').addClass('locked');

            // Save scrollposition for offset
            var windowScrollTop = window.pageYOffset || document.documentElement.scrollTop;

            // Calculate dimensions for imagebox's parent: .item and make fullscreen
            this.parentElement.css({
                'margin-left'   : -this.offsetLeft,
                'margin-top'    : -this.offsetTop + windowScrollTop,
            }).addClass('fullscreen');

        },

        resetImageContainers: function(){

            console.log('%c Reset Img Container ', 'background-color: #e74c3c; color: #ffffff;');

            // Helpers
            var self = this;
            var parent = this.parentElement;
            var imageBox = parent.find('.image-box');

            // Remove helper classes
            $('body').removeClass('locked');
            $('.item').removeClass('fullscreen top-layer');

            // Reset item position in grid
            parent.css({
                'margin-left'   : 0,
                'margin-top'    : 0
            }).removeClass('hidden');

            // Show hover button
            parent.find('.hover-btn').removeClass('hidden');
            
            // If an image is fullscreen, remove class to make it fade-out{@_grid.scss}
            if( imageBox.hasClass('active') ){
                setTimeout(function(){
                    imageBox.removeClass('active');
                }, 700);
            }
            
        },

    };
    
    return Grid; 
});