require.config({
    baseUrl: "js/bower_components/",
    paths: {
        app: '../app',
        plugins: '../plugins/',
        widgets: '../widgets/',
    },
    shim: {
        // switch to jquery if neccesary
        'zepto/zepto': {
            exports: "$"
        },       
        'modernizr/modernizr': {
            exports: 'Modernizr'
        }
    }
});

require(["app"], function(App) {

    window.App = App;

    App.init();

});