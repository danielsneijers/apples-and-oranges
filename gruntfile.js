module.exports = function(grunt) {

  grunt.initConfig({
    compass: {
      dist: {
        options: {
          config: 'config.rb',
          'output-style': 'compressed'
        }
      },
      dev: {
        options: {
          config: 'config.rb',
          'output-style': 'expanded',
          watch: true
        }
      }
    },
    requirejs: {
      compile: {
        options: {
          baseUrl: "./js/bower_components/",
          mainConfigFile: "js/main.js",
          include: "../main",
          name: "./almond/almond",
          out: "js/built/main.min.js",
          preserveLicenseComments: true
        }
      }
    },
    clean: {
        css: ['css']
    },
    imagemin: {                          // Task
      compressmedia: {                         // Another target
        files: [{
          expand: true,                  // Enable dynamic expansion
          cwd: 'images/',               // Src matches are relative to this path
          src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
          dest: 'images/'                  // Destination path prefix
        }]
      },
      dist: {                         // Another target
        files: [{
          expand: true,                  // Enable dynamic expansion
          cwd: 'images/',               // Src matches are relative to this path
          src: ['sprites-*.png'],   // Actual patterns to match
          dest: 'images/'                  // Destination path prefix
        }]
      }
    }
  });

  // Displays the elapsed execution time of grunt tasks
  require('time-grunt')(grunt);

  // Load tasks
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-imagemin');

  // Tasks
  grunt.registerTask('dev', ['clean:css','compass:dev']);
  grunt.registerTask('dist', ['clean:css','compass:dist', 'requirejs:compile', 'imagemin:dist']);
  grunt.registerTask('img', ['imagemin:compressmedia']);

};